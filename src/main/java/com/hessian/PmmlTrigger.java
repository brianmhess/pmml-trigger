package com.hessian;

/*

cqlsh -e "CREATE TABLE test.iris_with_class(id INT, sepal_length DOUBLE, sepal_width DOUBLE, petal_length DOUBLE, petal_width DOUBLE, species_label TEXT, prob_setosa DOUBLE, prob_versicolor DOUBLE, prob_virginica DOUBLE, predicted_species TEXT, PRIMARY KEY ((id)));"

cqlsh -e "CREATE TRIGGER pmml_predict_iris ON test.iris_with_class USING 'hessian.PmmlTrigger';"

cqlsh -e "ALTER TABLE test.iris_with_class WITH COMMENT = 'pmmlFileName=/home/digger/iris_rf.pmml';"

cassandra-loader -host localhost -f dev/jpmml-example2/src/main/resources/hessian/iris_with_id.csv -schema "test.iris_with_class(id,sepal_length,sepal_width,petal_length,petal_width,species)" -skipRows 1

 */

import java.nio.ByteBuffer;
import java.util.*;
import org.apache.cassandra.triggers.ITrigger;
import org.apache.cassandra.db.Mutation;
import org.apache.cassandra.db.Clustering;
import org.apache.cassandra.schema.*;
import org.apache.cassandra.db.rows.*;
import org.apache.cassandra.db.marshal.*;
import org.apache.cassandra.db.partitions.*;
import org.apache.cassandra.cql3.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.*;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import org.xml.sax.SAXException;
import javax.xml.bind.JAXBException;
import javax.xml.transform.sax.SAXSource;

import org.dmg.pmml.FieldName;
import org.dmg.pmml.Model;
import org.dmg.pmml.PMML;
import org.jpmml.evaluator.InputField;
import org.jpmml.evaluator.TargetField;
import org.jpmml.evaluator.FieldValue;
import org.jpmml.evaluator.ModelEvaluationContext;
import org.jpmml.evaluator.Evaluator;
import org.jpmml.evaluator.ModelEvaluator;
import org.jpmml.evaluator.ModelEvaluatorFactory;
import org.jpmml.evaluator.ValueFactoryFactory;
import org.jpmml.evaluator.ReportingValueFactoryFactory;
import org.jpmml.evaluator.ProbabilityDistribution;

/**
 *
 */

public class PmmlTrigger implements ITrigger {

    public PmmlTrigger() { evaluator = null; pmmlFileName = null; }
    private Evaluator evaluator;
    private String pmmlFileName_arg = "pmmlFileName=";
    private String pmmlFileName;
    
    public void initialize() {
	PMML pmml = null;
	try {
	    //String pmml_fname = "/home/digger/iris_rf.pmml";
	    pmml = createPMMLfromFile(pmmlFileName);
	}
	catch (Exception e) { }

	ModelEvaluatorFactory modelEvaluatorFactory = ModelEvaluatorFactory.newInstance();

	// Activate the generation of MathML prediction reports
	ValueFactoryFactory valueFactoryFactory = ReportingValueFactoryFactory.newInstance();
	modelEvaluatorFactory.setValueFactoryFactory(valueFactoryFactory);
	evaluator = (Evaluator)modelEvaluatorFactory.newModelEvaluator(pmml);
	evaluator.verify();
    }

    public Collection<Mutation> augmentNonBlocking(Partition partition)
    {
	if (null == evaluator) {
	    return Collections.emptyList();
	}
	PartitionUpdate.SimpleBuilder pusb = PartitionUpdate.simpleBuilder(partition.metadata(),
									   partition.partitionKey().getKey());

	// Get TableMetadata, ColumnMetadata, etc
	TableMetadata tmeta = partition.metadata();
	Set<String> columnNames = new HashSet<String>();
	for (ColumnMetadata cmeta : tmeta.columns()) {
	    columnNames.add(cmeta.name.toString());
	}

	// See if the pmmlFileName changed - if so, re-initialize.
	String nowPmmlFileName = null;
	String comment = tmeta.params.comment;
	if ("" != comment) {
	    String[] elems = comment.split(",");
	    for (String elem : elems) {
		if (elem.startsWith(pmmlFileName_arg)) {
		    nowPmmlFileName = elem.substring(pmmlFileName_arg.length());
		}
	    }
	}
	if ((nowPmmlFileName != null) && (!nowPmmlFileName.equals(pmmlFileName))) {
	    pmmlFileName = nowPmmlFileName;
	    initialize();
	}
	
	// Iterate through Rows in Partition
	UnfilteredRowIterator iter = partition.unfilteredIterator();
	while (iter.hasNext()){
	    Unfiltered u = iter.next();
	    // Only look at Rows (not Range Tombstone Marker)
	    if (!u.isRow()) continue;
	    Row row = (Row)u;
	    
	    // Get Arguments
	    Map<FieldName, FieldValue> arguments = new LinkedHashMap<FieldName, FieldValue>();

	    List<InputField> inputFields = evaluator.getInputFields();
	    for(InputField inputField : inputFields){
		FieldName inputFieldName = inputField.getName();
		ColumnMetadata cmeta = tmeta.getColumn(new ColumnIdentifier(inputFieldName.toString(), false));
		Cell cell = row.getCell(cmeta);
		ByteBuffer bb = cell.value();
		
		Object rawValue = cmeta.cellValueType().compose(bb);
		FieldValue inputFieldValue = inputField.prepare(rawValue);

		arguments.put(inputFieldName, inputFieldValue);
	    }

	    // Score model
            Map<FieldName, ?> results = evaluator.evaluate(arguments);

	    // Produce outputs
	    Clustering clustering = row.clustering();
	    Row.SimpleBuilder rsb = pusb.row(clustering);
	    for (FieldName fn : results.keySet()) {
		ghettoLog("FieldName: " + fn.toString());
		if (columnNames.contains(fn.toString())) {
		    ColumnMetadata cmeta = tmeta.getColumn(new ColumnIdentifier(fn.toString(), false));
		    rsb.add(fn.toString(), results.get(fn));
		}
	    }
	}

	return Collections.singletonList(pusb.buildAsMutation());
    }

    public PMML createPMMLfromFile(String fileName) throws SAXException, JAXBException, FileNotFoundException {
        File pmmlFile = new File(fileName);
	ghettoLog("fileName: " + fileName + "\n");
        String pmmlString = new Scanner(pmmlFile).useDelimiter("\\Z").next();
	ghettoLog("pmmlString:\n" + pmmlString);

        InputStream is = new ByteArrayInputStream(pmmlString.getBytes());
	PMML retPmml = null;
	try {
	    retPmml = org.jpmml.model.PMMLUtil.unmarshal(is);
	}
	catch (Exception e) {
	    ghettoLog("Message: " + e.getMessage());
	    ghettoLog(Arrays.toString(e.getStackTrace()));
	}
	return retPmml;
    }

    private void ghettoLog(String s) {
	try {
	    Files.write(Paths.get("/tmp/logit"), Arrays.asList(s), StandardOpenOption.APPEND);
	}
	catch (Exception e) { }
    }
}
