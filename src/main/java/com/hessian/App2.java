package com.hessian;

/*
java -cp target/hessian-pmml-0.1.jar com.hessian.App2 /home/digger/iris_rf.pmml "5.1,3.5,1.4,0.2,Iris-setosa"

 */

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import org.xml.sax.SAXException;
import javax.xml.bind.JAXBException;
import javax.xml.transform.sax.SAXSource;

import org.dmg.pmml.FieldName;
import org.dmg.pmml.Model;
import org.dmg.pmml.PMML;
import org.jpmml.evaluator.InputField;
import org.jpmml.evaluator.TargetField;
import org.jpmml.evaluator.FieldValue;
import org.jpmml.evaluator.ModelEvaluationContext;
import org.jpmml.evaluator.Evaluator;
import org.jpmml.evaluator.ModelEvaluator;
import org.jpmml.evaluator.ModelEvaluatorFactory;
import org.jpmml.evaluator.ValueFactoryFactory;
import org.jpmml.evaluator.ReportingValueFactoryFactory;
import org.jpmml.evaluator.ProbabilityDistribution;

/**
 *
 */

public class App2 {

    public App2(){}

    public static void main(String... args) throws Exception {
        App2 app = new App2();
        app.run(args);
    }

    public void run(String ... args) throws Exception {
	String pmml_fname = args[0];
	String line = args[1];
        PMML pmml = createPMMLfromFile(pmml_fname);
        //PMML pmml = createPMMLfromFile("iris_rf.pmml");

	ModelEvaluatorFactory modelEvaluatorFactory = ModelEvaluatorFactory.newInstance();

	// Activate the generation of MathML prediction reports
	ValueFactoryFactory valueFactoryFactory = ReportingValueFactoryFactory.newInstance();
	modelEvaluatorFactory.setValueFactoryFactory(valueFactoryFactory);
	Evaluator evaluator = (Evaluator)modelEvaluatorFactory.newModelEvaluator(pmml);
	evaluator.verify();

        Map<FieldName, FieldValue> arguments = new LinkedHashMap<FieldName, FieldValue>();
        String[] lineArgs = line.split(",");
        if( lineArgs.length != 5) {
	    System.err.println("Must specify 5 arguments");
	    System.exit(-1);
	}
	Map<FieldName,String> stringArgs = new LinkedHashMap<FieldName,String>();
	//stringArgs.put(new FieldName("Sepal.Length"), lineArgs[0]);
	//stringArgs.put(new FieldName("Sepal.Width") , lineArgs[1]);
	//stringArgs.put(new FieldName("Petal.Length"), lineArgs[2]);
	//stringArgs.put(new FieldName("Petal.Width") , lineArgs[3]);
	stringArgs.put(new FieldName("sepal_length"), lineArgs[0]);
	stringArgs.put(new FieldName("sepal_width") , lineArgs[1]);
	stringArgs.put(new FieldName("petal_length"), lineArgs[2]);
	stringArgs.put(new FieldName("petal_width") , lineArgs[3]);

	List<InputField> inputFields = evaluator.getInputFields();
	//for (InputField if : inputFields) {
	for (int i = 0; i < inputFields.size(); i++) {
	    //InputField if = inputFields.get(i);
	    FieldName ifn  = inputFields.get(i).getName();
	    FieldValue ifv = inputFields.get(i).prepare(stringArgs.get(ifn));
	    arguments.put(ifn,ifv);
	}

	Map<FieldName, ?> results = evaluator.evaluate(arguments);
	System.out.println("\nResults:");
	System.out.println(results);

	List<TargetField> targetFields = evaluator.getTargetFields();
	Map<FieldName,Object> targetMap = new LinkedHashMap<FieldName,Object>();
	for(TargetField targetField : targetFields){
	    FieldName targetFieldName = targetField.getName();
	    System.out.println(targetFieldName + ":");
	    Object targetFieldValue = results.get(targetFieldName);
	    System.out.println(targetFieldValue);
	    targetMap.put(targetFieldName,targetFieldValue);
	}
	//Object opd = targetMap.get(new FieldName("species"));
	//System.out.println("\nopd:\n" + opd);

	System.out.println("\ntargetFields:\n" + evaluator.getTargetFields());
	System.out.println("\noutputFields:\n" + evaluator.getOutputFields());
	
	System.out.println("\np(setosa) = " + results.get(new FieldName("prob_setosa")));
	System.out.println("p(versicolor) = " + results.get(new FieldName("prob_versicolor")));
	System.out.println("p(virginica) = " + results.get(new FieldName("prob_virginica")));
	System.out.println("prediction = " + results.get(new FieldName("predicted_species")));
       
    }


    public PMML createPMMLfromFile(String fileName) throws SAXException, JAXBException, FileNotFoundException {
        File pmmlFile = new File(fileName);
        String pmmlString = new Scanner(pmmlFile).useDelimiter("\\Z").next();

        InputStream is = new ByteArrayInputStream(pmmlString.getBytes());
        return org.jpmml.model.PMMLUtil.unmarshal(is);
    }
}
